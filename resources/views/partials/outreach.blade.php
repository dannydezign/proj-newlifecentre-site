<section class="donate-section spad set-bg" data-setbg="img/donate-bg.jpg" style="background-image: url(&quot;img/donate-bg.jpg&quot;);">
    <div class="container">
        <div class="row">
             
            <div class="col-md-6 col-lg-5">
           
                <img src="{{ asset('img/event/1.jpg') }}" />
            </div>
            <div class="col-md-6 col-lg-7 donate-content text-right">
                <h2>SOCIAL OUTREACH</h2>
               
                <p>Our vision is to reach out, give hope, rehabilitate, educate, and empower impoverished persons in order to achieve individual </p>
                <p>and community transformation</p> 
                <br>
                <br>
                <a href="/" class="site-btn sb-wide sb-line">read more</a>
            </div>
        </div>
    </div>
</section>