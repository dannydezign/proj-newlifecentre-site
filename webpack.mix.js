const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js').postCss('resources/css/app.css', 'public/css', [
    require('postcss-import'),
    require('tailwindcss'),
    require('autoprefixer'),
]);

mix.styles([
    'files/css/style.css',
], 'public/css/all.css');

mix.js([
    'files/js/main.js',
    'files/js/map.js',
    'resources/js/youtube.js',
    'resources/js/image-popup.js',
], 'public/js/all.js');


mix.js([
    'resources/js/react/getYoutubeVideos.js',
], 'public/js/react');


mix.copy('files/img/', 'public/img', false);
mix.copy('files/fonts/', 'public/fonts', false);