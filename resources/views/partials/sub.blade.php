    <section class="event-list-section spad">
      <div class="container mt-2">
        <div class="row">
        @foreach (App\Http\Services\Home::getSubInformation() as $sub)
        
          <div class="col-md-3 col-sm-6 item">
            <div class="card item-card card-block">
              <img class="box" src="{{ $sub['image'] }}" alt="{{ $sub['btnText'] }}">
              <a href="{{ $sub['link'] }}" class="site-btn sb-wide sb-line subbtn">{{ $sub['btnText'] }}</a>
            </div>
          </div>
          
        @endforeach
        </div>
      </div>
    </section> 
 
    