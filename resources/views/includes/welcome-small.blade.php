    
	<!-- About section -->
	<section class="about-section spad">
		<div class="container">
			<div class="row">
				<div class="col-md-6 about-content">
					<h2>Welcome to Newlife Centre</h2>
					<p>{!! $aboutUsMini !!}</p>
					<a href="" class="site-btn sb-wide">join with us</a>
				</div>
				<div class="col-md-6 about-img">
					<img src="img/about.jpg" alt="">
				</div>
			</div>		
		</div>
	</section>
    <!-- About section end -->