<?php

namespace App\Http\Services;


class Home
{
   
    public static function getSubInformation()
    {
       return [
            [
                'image'     => 'https://static.pexels.com/photos/7096/people-woman-coffee-meeting.jpg',
                'btnText'   => 'giving',
                'link'      => 'giving'
            ],
            [
                'image'     => 'https://static.pexels.com/photos/7096/people-woman-coffee-meeting.jpg',
                'btnText'   => 'live streaming',
                'link'      => 'streaming'
            ],
            [
                'image'     => 'https://static.pexels.com/photos/7096/people-woman-coffee-meeting.jpg',
                'btnText'   => 'bible study',
                'link'      => 'bible-study'
            ],
            [
                'image'     => 'https://static.pexels.com/photos/7096/people-woman-coffee-meeting.jpg',
                'btnText'   => 'booking reading',
                'link'      => 'booking-reading'
            ] 
        ];
    }

}
