@extends('layouts.main')

@section('content')

@include('includes.title-section', ['title' => 'Sermon'])
 
<section class="sermons-list-section spad">
    <div class="container">
        <div class="section-title">
            <span>Experience God's Presence</span>
            <h2>Latest Sermons</h2>
        </div>
        <div class="row">
        <div id="getYoutubeVideo"></div>
        </div>
    </div>
</section> 

{{-- @include('partials.sermons') --}}
        
@endsection