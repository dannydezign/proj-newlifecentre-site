<section class="page-info-section set-bg" data-setbg="img/bg.jpg">
		<div class="page-info-content">
			<div class="pi-inner">
				<div class="container">
					<h2 class="text-capitalize">{{ $title ?? '' }}</h2>
					<div class="site-breadcrumb">
						<a href="#">Home</a> <i class="fa fa-angle-right"></i>
						<span class="text-capitalize">{{ $title ?? '' }}</span>
					</div>
				</div>
			</div>
		</div>
	</section>