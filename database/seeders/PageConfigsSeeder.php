<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Pages;
use App\Models\PageConfigs;

class PageConfigsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pageConfigs = [
            (object) [
                'name' => 'about',
                'configs_name' => 'about-us',
                'configs_value' => '<p>For God did not send his Son into the world to condemn the world, but to save the world through him. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia dese mollit anim id est laborum. Sed ut perspiciatis unde omnis iste.</p>'
            ],
            (object) [
                'name' => 'about',
                'configs_name' => 'about-us-main',
                'configs_value' => '<p>Newlife Centre is part of the RCCG (Redeemed Christian Church of God) network in the UK, a bible believing family church,with a cordial interaction and relationship amongst members.Our church is passionate about serving and helping families to build sustainable lives to stay and thrive in the city.</p>'
            ],
            (object) [
                'name' => 'about',
                'configs_name' => 'about-us-value',
                'configs_value' => '<p>We believe Jesus Christ is the only begotten Son of God. </p><p>We believe in the trinity; one God who exists in three distinct persons; Father, Son and the Holy Spirit. </p><p>We believe in the Death and Resurrection of Christ. </p><p>We believe in the person of the Holy Spirit, His Fruit and Gifts. </p><p>We believe in the second coming of Christ. </p><p>We believe in the Bible as the foundation and ultimate compass for a successful Christian life. </p>'
            ],
            (object) [
                'name' => 'about',
                'configs_name' => 'about-us-vision',
                'configs_value' => '<p>To see believers come to maturity of the faith and reign in life as prince and princess of God. </p><p>We have a mandate from God to build men and women up in the Christian faith, equipping them with the word of God and the power of the Holy Spirit, so they can be maximally effective in what God has called them to do.</p>'
            ],
            (object) [
                'name' => 'about',
                'configs_name' => 'about-us-mission',
                'configs_value' => '<p>To nurture the life of God in other believers and to raise men and women who are committed to the body of Christ.<p> <p>Our commitment is to love, encourage, collaboratively nurture the fruit of the Spirit,</p><p>serve and to share our possessions and our hearts with each other (1 Cor 12 and Gal 5:13-26).</p>'
            ],
        ];


        foreach ($pageConfigs as $configs)
        {
           
            $pageId = Pages::updateOrCreate(
                ['name' => $configs->name]
            );
    
            PageConfigs::updateOrCreate(
            [
                'page_id' => $pageId->id,
                'name' => $configs->configs_name,
            ],
                [
                'page_id' => $pageId->id,
                'name' => $configs->configs_name,
                'value' => $configs->configs_value
            ]);
        }
    }
}
