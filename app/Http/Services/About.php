<?php

namespace App\Http\Services;
use App\Models\Pages as Page;
use App\Models\PageConfigs;

class About
{

    public static function getAboutUs() {    
        $id = Page::where('name', 'about')->pluck('id')->first();
        return PageConfigs::where('page_id', $id)->where('name', 'about-us-main')->pluck('value')->first();
    }

    public static function getAboutUsValue() {    
        $id = Page::where('name', 'about')->pluck('id')->first();
        return PageConfigs::where('page_id', $id)->where('name', 'about-us-value')->pluck('value')->first();
    }

    public static function getAboutUsVision() {    
        $id = Page::where('name', 'about')->pluck('id')->first();
        return PageConfigs::where('page_id', $id)->where('name', 'about-us-vision')->pluck('value')->first();
    }

    public static function getAboutUsMission() {    
        $id = Page::where('name', 'about')->pluck('id')->first();
        return PageConfigs::where('page_id', $id)->where('name', 'about-us-mission')->pluck('value')->first();
    }
 
}
