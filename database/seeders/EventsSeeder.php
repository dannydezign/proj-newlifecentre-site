<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Events;

class EventsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $events = [
            (object) [
                'name' => 'Golf Day',
                'description' => 'Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis.',
                'date_start' => '2020-12-19 00:00:00',
                'date_end' => '2020-12-19 00:30:00',
                'venue' => 'golf yard',
                'image' => 'red',
                'status' => 1,
                'link' => 'https://us02web.zoom.us/j/3411331977?pwd=SDEvYnJMMXJGL0ZCNkFKR3d6ZSthQT09'
            ],
            (object) [
                'name' => 'Thanksgiving Cross Over',
                'description' => 'Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis.',
                'date_start' => '2021-12-29 11:32:19',
                'date_end' => '2021-12-29 11:32:19',
                'venue' => 'venue',
                'image' => 'red',
                'status' => 1,
                'link' => 'https://us02web.zoom.us/j/3411331977?pwd=SDEvYnJMMXJGL0ZCNkFKR3d6ZSthQT09'
            ],
            (object) [
                'name' => 'Children Day Party',
                'description' => 'Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis.',
                'date_start' => '2021-01-10 11:32:19',
                'date_end' => '2021-01-10 11:32:19',
                'venue' => 'venue 1',
                'image' => 'red 1',
                'status' => 1,
                'link' => 'https://us02web.zoom.us/j/3411331977?pwd=SDEvYnJMMXJGL0ZCNkFKR3d6ZSthQT09'
            ],
            (object) [
                'name' => 'Men Tennis Competition',
                'description' => 'Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis.',
                'date_start' => '2020-12-30 11:32:19',
                'date_end' => '2020-12-30 11:32:19',
                'venue' => 'venue 2',
                'image' => 'image 2',
                'status' => 1,
                'link' => 'https://us02web.zoom.us/j/3411331977?pwd=SDEvYnJMMXJGL0ZCNkFKR3d6ZSthQT09'
            ],
            (object) [
                'name' => 'Women Talk',
                'description' => 'Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis.',
                'date_start' => '2020-12-22 11:32:19',
                'date_end' => '2020-12-22 11:32:19',
                'venue' => 'venue 3',
                'image' => 'red 3',
                'status' => 1,
                'link' => 'https://us02web.zoom.us/j/3411331977?pwd=SDEvYnJMMXJGL0ZCNkFKR3d6ZSthQT09'
            ],
        ];


        foreach ($events as $event)
        { 
            Events::updateOrCreate(
            [
                'name' => $event->name
            ],
            [
                'name'        => $event->name,
                'description' => $event->description,
                'image'       => $event->image,
                'display'     => $event->status,
                'date_start'  => $event->date_start,
                'date'        => date("Y-m-d", strtotime($event->date_start)),
                'date_end'    => $event->date_end,
                'venue'       => $event->venue,
                
            ]);
        }
    }
}   
