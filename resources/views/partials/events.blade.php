  <!-- Event list section -->
  <section class="event-list-section spad">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="section-title">
						<span>Experience God's Presence</span>
						<h2>Upcoming Events</h2>
					</div>
				</div>
				<div class="col-md-6 text-right event-more">
					<a href="" class="site-btn">view all events</a>
				</div>
			</div>
			<div class="event-list">
				<!-- event list item -->
				@forelse ($events as $event)
					<!-- event list item -->
					@include('components.custom.event' ,['event' => $event])
				@empty
					{{-- @include('partials.alerts' ,['alert' => ['warning' => 'No Upcoming Event']]) --}}
					<img src="{{ asset('img/no-event-image.png') }}" />
				@endforelse
			</div>
		</div>
	</section>
	<!-- Event list section end-->