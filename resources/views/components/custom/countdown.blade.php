<div class="counter" id="countdown">
    <ul>
      <li class="counter-item"><h4 id="days"></h4>Days</li>
      <li class="counter-item"><h4 id="hours"></h4>Hours</li>
      <li class="counter-item"><h4 id="minutes"></h4>Mins</li>
      <li class="counter-item"><h4 id="seconds"></h4>Secs</li>
    </ul>
</div>
 