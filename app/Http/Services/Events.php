<?php

namespace App\Http\Services;
use App\Models\Events as EventModel;


class Events
{
   
    public static function getLatestFiveEvents() {
        $now = date("Y-m-d h:i:s");

        $eventsList = EventModel::status(1)->where('date_start', '>', $now)->orderby('date_start', 'asc')->get();
        $events = [];

        foreach ($eventsList as $event)
        {
            $events[] = (object) [
                'name'           => $event->name,
                'description'    => $event->description,
                'venue'          => $event->venue,
                'date'           => date("F, jS Y - h:i A", strtotime($event->date_start)),
                'date_num'       => date("j", strtotime($event->date_start)),
                'date_mon'       => date("M", strtotime($event->date_start)),
            ];
        }

        return $events;
 
    }

    public static function getNextEvent()
    {
        $event = [];
        $now = date("Y-m-d");
        $tomorrow = date('Y-m-d', strtotime($now. ' +1 days'));
        $later = date('Y-m-d', strtotime($now. ' +4 days'));
        $recursiveDates = [self::getSunday()->date, self::getThursday()->date, self::getTuesday()->date, self::getWednesday()->date, self::getMonday()->date];
        $recursiveDatesSort = self::sort($recursiveDates);
        // $chechForToday = EventModel::status(1)->where('date', '=', $now)->orderby('date_start', 'asc')->first();
        
        // Check if today has an event.
        if ($chechForToday = EventModel::status(1)->where('date', '=', $now)->orderby('date_start', 'asc')->first() ?? null) {
            // $event = $chechForToday;
            $nowMain = date('Y-m-d H:i:s');
            $endDate = date('Y-m-d H:i:s', strtotime($chechForToday->date_end));
            
            if ($nowMain > $endDate) {
                $nextRecusiveEvent = $recursiveDatesSort[0];
                $nextDBEvent = EventModel::status(1)->where('date', '>', $now)->orderby('date_start', 'asc')->first();
                $event = ($nextDBEvent->date < $nextRecusiveEvent) ? $nextDBEvent : $nextRecusiveEvent = self::getDate($nextRecusiveEvent);
            } else {
                $event = $chechForToday;
            }
        } else {
            $nextRecusiveEvent = $recursiveDatesSort[0];
            $nextDBEvent = EventModel::status(1)->where('date', '>', $now)->orderby('date_start', 'asc')->first();
            $event = ($nextDBEvent->date < $nextRecusiveEvent) ? $nextDBEvent : $nextRecusiveEvent = self::getDate($nextRecusiveEvent);
        }
         
        return (object) [
            'name'           => $event->name,
            'description'    => $event->description,
            'venue'          => $event->venue,
            'date'           => date("F, jS Y - h:i A", strtotime($event->date_start)),
            'date_num'       => date("j", strtotime($event->date)),
            'date_mon'       => date("M", strtotime($event->date)),
            'countdown_date' => date("M j, Y H:i:s", strtotime($event->date_start)),
            'endEvent_date' => date("M j, Y H:i:s", strtotime($event->date_end)),
        ];
    }

    protected static function getDate($date)
    { 
        $timestamp = strtotime($date);
        $day = date('D', $timestamp);

        switch ($day) {
            case "Sun":
                $event = self::getSunday();
                break;
            case "Thu":
                $event = self::getThursday();
            case "Wed":
                $event = self::getWednesday();
                break;
            case "Tue":
                $event = self::getTuesday();
                break;
            case "Mon":
                $event = self::getMonday();
                break;
            default:
                $event = null;
        }

        return $event;
      
    }

    protected static function getSunday()
    {
        $timestamp = time();
        $isSunday = date('D', $timestamp) === 'Sun';
        $nextSunday = date('Y-m-d', strtotime('next sunday'));
        $date = $isSunday ? date("Y-m-d") : $nextSunday;
        return (object) [
            'name'           => 'Sunday Service',
            'description'    => 'Sunday Service',
            'date'           =>  $date,
            'date_start'     =>  $date . ' 11:30:00',
            'venue'          => 'Zoom',
            'date_end'       =>  $date . ' 13:30:00',
        ];
    }


    protected static function getThursday()
    {
        $timestamp = time();
        $isThursday = date('D', $timestamp) === 'Thu';
        $nextThursday = date('Y-m-d', strtotime('next thursday'));
        $date = $isThursday ? date("Y-m-d") : $nextThursday;
        return (object) [
            'name'           => 'Bible Study',
            'description'    => 'Bible Study',
            'date'           =>  $date,
            'venue'          => 'Zoom',
            'date_start'     =>  $date . ' 08:00:00',
            'date_end'       =>  $date . ' 10:00:00',
        ];
    }

    protected static function getTuesday()
    {
        $timestamp = time();
        $isTuesday = date('D', $timestamp) === 'Tue';
        $nextTuesday = date('Y-m-d', strtotime('next tuesday'));
        $date = $isTuesday ? date("Y-m-d") : $nextTuesday;
        return (object) [
            'name'           => 'Church Prayer',
            'description'    => 'Church Prayer',
            'date'           =>  $date,
            'venue'          => 'Zoom',
            'date_start'     =>  $date . ' 20:30:00',
            'date_end'       =>  $date . ' 21:30:00',
        ];
    }

    protected static function getMonday()
    {
        $timestamp = time();
        $isTuesday = date('D', $timestamp) === 'Tue';
        $nextTuesday = date('Y-m-d', strtotime('next tuesday'));
        $date = $isTuesday ? date("Y-m-d") : $nextTuesday;
        return (object) [
            'name'           => "Men's Prayer",
            'description'    => "Men's Prayer",
            'date'           =>  $date,
            'venue'          => 'Zoom',
            'date_start'     =>  $date . ' 08:30:00',
            'date_end'       =>  $date . ' 09:30:00',
        ];
    }

    protected static function getWednesday()
    {
        $timestamp = time();
        $isTuesday = date('D', $timestamp) === 'Tue';
        $nextTuesday = date('Y-m-d', strtotime('next tuesday'));
        $date = $isTuesday ? date("Y-m-d") : $nextTuesday;
        return (object) [
            'name'           => "Women's Prayer",
            'description'    => "Women's Prayer",
            'date'           =>  $date,
            'venue'          => 'Zoom',
            'date_start'     =>  $date . ' 18:00:00',
            'date_end'       =>  $date . ' 19:00:00',
        ];
    }

    // protected static function dateRange( $first, $last, $step = '+2 day', $format = 'Y-m-d' )
    // {
    //     $dates = [];
    //     $current = strtotime( $first );
    //     $last = strtotime( $last );
    
    //     while( $current <= $last ) {
    //         dd(date( $format, $current ));
    //         // $dates[] = $current;
    //         // $dates[] = date( $format, $current );
    //         // $current = strtotime( $step, $current );
    //     }
    
    //     return $dates;
    // }

    protected static function checkDateInDates($date, $dateArray)
    { 
        if (in_array($date, $dateArray)) {
            $value = $date;
        } else {
            $value = false;
        }

        return $value; 
    }

    protected static function getNextDate($dates, $start, $end)
    {
        $startT = strtotime($start);
        $endT = strtotime($end);
        foreach($dates as $date) {
            $timestamp = strtotime($date);
            if($timestamp >= $startT && $timestamp <= $endT) {
                $p[] = $date;
            break;
            }
        }

        return $p[0];
    }

    protected static function sort($arr)
    {
        function date_sort($a, $b) {
            return strtotime($a) - strtotime($b);
        }
        usort($arr, "App\Http\Services\date_sort");

        return $arr;
    }

}
