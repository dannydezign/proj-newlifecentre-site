<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AboutController;
use App\Http\Controllers\EventController;
use App\Http\Controllers\GivingController;
use App\Http\Controllers\SermonController;
use App\Http\Controllers\HolySpiritExperienceController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index']);

Route::get('/home', [HomeController::class, 'index']);

Route::get('/about', [AboutController::class, 'index']);

// Route::get('/sermon', [AboutController::class, 'index']);

Route::get('/events', [EventController::class, 'index']);

Route::get('/giving', [GivingController::class, 'index']);

Route::get('/sermons', [SermonController::class, 'index']);

Route::get('/holyspiritexperience', [HolySpiritExperienceController::class, 'index']);



Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');


require __DIR__.'/auth.php';
