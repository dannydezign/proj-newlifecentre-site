<!DOCTYPE html>
<html lang="en">
<head>
	<title>LibChurch - Event Template</title>
	<meta charset="UTF-8">
	<meta name="description" content="LibChurch Event Template">
	<meta name="keywords" content="event, libChurch, creative, html">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Favicon -->   
	<link href="img/favicon.ico" rel="shortcut icon"/>

	<!-- Google Fonts -->
    @include('partials.fonts')
    @include('partials.stylesheet')
	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<script src="https://unpkg.com/react@17/umd/react.development.js"></script>
    <script src="https://unpkg.com/react-dom@17/umd/react-dom.development.js"></script>

    <!-- Don't use this in production: -->
    <script src="https://unpkg.com/@babel/standalone/babel.min.js"></script>
</head>
<body>
	@include('partials.preloader')
	@include('includes.event-countdown', ['event' => App\Http\Services\Events::getNextEvent()])

	@include('partials.navigation', ['data' => App\Http\Services\Navigation::getNavigation()])
	@yield('content')
	@include('includes.subscribe-box')

	@include('partials.footer')
    @include('partials.scripts')
</body>
</html>