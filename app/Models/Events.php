<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Events extends Model
{
    use HasFactory;

    public static function scopeStatus($query, $display)
    {
        $query->where('display', (int) $display)->get();
    }
}
