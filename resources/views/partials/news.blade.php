    	<!-- Blog section -->
        <section class="blog-section spad">
		<div class="container">
			<div class="section-title">
				<span>Experience God's Presence</span>
				<h2>LATEST NEWS</h2>
			</div>
			<div class="row">
				<div class="col-sm-6 col-md-4">
					<div class="blog-item">
						<div class="bi-thumb set-bg" data-setbg="img/blog/1.jpg"></div>
						<div class="bi-content">
							<div class="date">On Sunday 6 January, 2019</div>
							<h4><a href="single-blog.html">RCCG Fasting starts on Friday 7th January</a></h4>
							<div class="bi-author">by Media Team</div>
							<a href="#" class="bi-cata">News</a>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-md-4">
					<div class="blog-item">
						<div class="bi-thumb set-bg" data-setbg="img/blog/2.jpg"></div>
						<div class="bi-content">
							<div class="date">On Tuesday 8 January, 2019</div>
							<h4><a href="single-blog.html">Prayer Meeting Every Tuesday</a></h4>
							<div class="bi-author">by Prayer Team</div>
							<a href="#" class="bi-cata">News</a>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-md-4">
					<div class="blog-item">
						<div class="bi-thumb set-bg" data-setbg="img/blog/3.jpg"></div>
						<div class="bi-content">
							<div class="date">On Sunday 13 January, 2019</div>
							<h4><a href="single-blog.html">Listen to the latest preaching</a></h4>
							<div class="bi-author">by Media Team</div>
							<a href="#" class="bi-cata">News</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Blog section end-->