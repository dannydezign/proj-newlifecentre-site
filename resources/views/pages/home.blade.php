@extends('layouts.main')

@section('content')
	
	@include('includes.welcome-theme')

	@include('includes.welcome-small', ['aboutUsMini' => $aboutUsMini])

	@include('partials.image-bg-display')
	
	@include('partials.sub')

	@include('partials.outreach')

	@include('partials.events', ['events' => $latestFiveEvents])

	
@endsection