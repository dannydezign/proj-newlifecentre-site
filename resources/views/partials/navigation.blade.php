<!-- Navigation Start -->
    <header class="header-section">
		<div class="container">		
			<!-- logo -->
			<a href="index.html" class="site-logo"><img src="img/logo1.png" alt=""></a>
			<!-- <a href="" class="site-btn hidden-xs">send donation</a> -->
			<!-- nav menu -->
			<div class="nav-switch">
				<i class="fa fa-bars"></i>
			</div>
			@include('components.custom.navigation', ['data' => $data])
		</div>
	</header>
<!-- Navigation End -->