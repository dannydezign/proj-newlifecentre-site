	<input id="event-countdown_date" type="hidden" class="form-check-input" value="{{ $event->countdown_date }}">
	<!-- Event section -->
	<section class="event-section" style="background: #337f98 !important">
		<div class="container">
			<div class="row">
				<div class="col-md-5 col-lg-6">
					<div class="event-info">
						<div class="event-date">
							<h2>{{ $event->date_num }}</h2>{{ $event->date_mon }}
						</div>
						<h3>{{ $event->name }}</h3>
						<p>
							<i class="fa fa-calendar"></i> {{ $event->date }} | <i class="fa fa-map-marker"></i> {{ $event->venue }} |  <i class="fas fa-video"></i></i> <a href="" >Read more</a></p>
					</div>
					
				</div>
			
				<div class="col-md-7 col-lg-6">
					<div id="countdown_box">
						@include('components.custom.countdown')
						
					</div>
					<div class="counter-content" style="display: none" id="content_box">
						Event Has Started
					  </div>
					  
				</div>
			</div>
		</div>
	</section>
<!-- Event section end --> 