/* =================================
------------------------------------
	LibChurch - Event Template
	Version: 1.0
 ------------------------------------ 
 ====================================*/



'use strict';

const { defaultsDeep } = require("lodash");

$(window).on('load', function () {
	/*------------------
		Preloder
	--------------------*/
	$(".loader").fadeOut();
	$("#preloder").delay(400).fadeOut("slow");

});

(function ($) {

	/*------------------
		Navigation
	--------------------*/
	$('.nav-switch').on('click', function (event) {
		$('.main-menu').slideToggle(400);
		event.preventDefault();
	});


	/*------------------
		Background set
	--------------------*/
	$('.set-bg').each(function () {
		var bg = $(this).data('setbg');
		$(this).css('background-image', 'url(' + bg + ')');
	});




})(jQuery);

(function () {

	const endDate = "Dec 21, 2020 20:00:00";
	const date = document.getElementById("event-countdown_date").value;
	// const date = "Dec 18, 2020 20:00:00";

	const second = 1000,
		minute = second * 60,
		hour = minute * 60,
		day = hour * 24;

	let birthday = date,
		countDown = new Date(birthday).getTime(),
		x = setInterval(function () {

			let now = new Date().getTime(),
				distance = countDown - now;

			document.getElementById("days").innerText = Math.floor(distance / (day)),
				document.getElementById("hours").innerText = Math.floor((distance % (day)) / (hour)),
				document.getElementById("minutes").innerText = Math.floor((distance % (hour)) / (minute)),
				document.getElementById("seconds").innerText = Math.floor((distance % (minute)) / second);
			let red = new Date(endDate).getTime();

			//do something later when date is reached
			if (distance < 0) {
				$('#countdown_box').hide();
				$('#content_box').show();
				if (red) {
					$('#content_box').text('EVENT FINISHED');
				}
				clearInterval(x);
			}
			//seconds
		}, 0)
}());

