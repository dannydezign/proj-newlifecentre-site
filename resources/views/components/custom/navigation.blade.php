<nav class="main-menu">
    <ul>
        @forelse ($data as $nav)
            @if ($nav['route'] == 'laravel')
                <li class="{{ $nav['show'] ? '' : 'd-none' }} {{ $nav['name'] == 'home' ? 'active' : '' }}"><a href="{{ $nav['link'] }}" target="_blank">{{ $nav['name'] }}</a></li>
            @elseif ($nav['route'] == 'youtube')
                <li class="{{ $nav['show'] ? '' : 'd-none' }} {{ $nav['name'] == 'home' ? 'active' : '' }}"><a href="{{ $nav['link'] }}" target="_blank">{{ $nav['name'] }}</a></li>
            @endif
        @empty
            <div class="alert alert-warning">
                <strong>Warning!</strong> Permission Required.
            </div>
        @endforelse
        
    </ul>
</nav>