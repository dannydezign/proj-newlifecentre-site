<div class="el-item">
    <div class="row">
        <div class="col-md-4">
            <div class="el-thubm set-bg" data-setbg="img/event/2.jpg"></div>
        </div>
        <div class="col-md-8">
            <div class="el-content">
                <div class="el-header">
                    <div class="el-date">
                        <h2>{{ $event->date_num }}</h2>{{ $event->date_mon }}
                    </div>
                    <h3>{{ $event->name }}</h3>
                    <div class="el-metas">
                        {{-- <div class="el-meta"><i class="fa fa-user"></i> Vincent John</div> --}}
                        <div class="el-meta"><i class="fa fa-calendar"></i> {{ $event->date }}  </div>
                        <div class="el-meta"><i class="fa fa-map-marker"></i> {{ $event->venue }}</div>
                    </div>
                </div>
                <p>{{ $event->description }}.</p>
                <a href="" class="site-btn sb-line">Read more</a>
            </div>
        </div>
    </div>
</div>