<section class="hero-section">
		  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
      <li data-target="#myCarousel" data-slide-to="3"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <div class="item active">
        <img src="img/bg1.jpg" alt="Los Angeles" style="width:100%;">
      </div>
	
      <div class="item">
        <img src="img/bg2.jpg" alt="Chicago" style="width:100%;">
      </div>
    
      <div class="item">
 		<img src="img/bg3.jpg" alt="Chicago" style="width:100%;">
      </div>

      <div class="item">
 		<img src="img/bg4.jpg" alt="Chicago" style="width:100%;">
      </div>
    </div>
 
  </div>