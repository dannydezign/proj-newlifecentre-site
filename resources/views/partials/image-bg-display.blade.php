<section class="donate-section spad set-bg" data-setbg="img/media-bg.jpg" style="background-image: url(&quot;img/donate-bg.jpg&quot;);">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-lg-7 donate-content">
                <h2>Media</h2>
               
                <p>Discover a wealth of biblical teaching in This </p>
                <p>Present House media library. Listen to the very </p>
                <p>latest messages.</p>
                <br>
                <br>
                <a href="/" class="site-btn sb-wide sb-line">watch and listen</a>
            </div>
            <div class="col-md-6 col-lg-5">
           
                <img src="{{ asset('img/camera2.png') }}" />
            </div>
        </div>
    </div>
</section>