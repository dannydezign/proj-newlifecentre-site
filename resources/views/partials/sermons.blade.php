<div class="container">
    <div class="section-title">
        <span>Experience God's Presence</span>
        <h2>Popular Sermons</h2>
    </div>
    <div class="row">
        <div class="col-sm-6 col-md-4">
            <div class="sermon-item">
                <div class="si-thumb set-bg" data-setbg="img/sermons/1.jpg" style="background-image: url(&quot;img/sermons/1.jpg&quot;);"></div>
                <div class="si-content">
                    <h4>I Will Be Gracious to Whom I Will Be Gracious</h4>
                    <ul class="sermon-info">
                        <li>Sermon From: <span>Vincent John</span></li>
                        <li>Categories: <span>God, Pray</span></li>
                        <li><span>On Monday 23 DEC, 2018</span></li>
                    </ul>
                    <div class="icon-links">
                        <a href=""><i class="ti-link"></i></a>
                        <a href=""><i class="ti-zip"></i></a>
                        <a href=""><i class="ti-headphone"></i></a>
                        <a href=""><i class="ti-import"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4">
            <div class="sermon-item">
                <div class="si-thumb set-bg" data-setbg="img/sermons/2.jpg" style="background-image: url(&quot;img/sermons/2.jpg&quot;);"></div>
                <div class="si-content">
                    <h4>Perseverance of the Saints</h4>
                    <ul class="sermon-info">
                        <li>Sermon From: <span>Vincent John</span></li>
                        <li>Categories: <span>God, Pray</span></li>
                        <li><span>On Monday 23 DEC, 2018</span></li>
                    </ul>
                    <div class="icon-links">
                        <a href=""><i class="ti-link"></i></a>
                        <a href=""><i class="ti-zip"></i></a>
                        <a href=""><i class="ti-headphone"></i></a>
                        <a href=""><i class="ti-import"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4">
            <div class="sermon-item">
                <div class="si-thumb set-bg" data-setbg="img/sermons/3.jpg" style="background-image: url(&quot;img/sermons/3.jpg&quot;);"></div>
                <div class="si-content">
                    <h4>I Will Be Gracious to Whom I Will Be Gracious</h4>
                    <ul class="sermon-info">
                        <li>Sermon From: <span>Vincent John</span></li>
                        <li>Categories: <span>God, Pray</span></li>
                        <li><span>On Monday 23 DEC, 2018</span></li>
                    </ul>
                    <div class="icon-links">
                        <a href=""><i class="fas fa-link"></i></a>
                        <a href=""><i class="ti-zip"></i></a>
                        <a href=""><i class="ti-headphone"></i></a>
                        <a href=""><i class="ti-import"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4">
            <div class="sermon-item">
                <div class="si-thumb set-bg" data-setbg="img/sermons/4.jpg" style="background-image: url(&quot;img/sermons/4.jpg&quot;);"></div>
                <div class="si-content">
                    <h4>The Lofty One Whose Name Is Holy</h4>
                    <ul class="sermon-info">
                        <li>Sermon From: <span>Vincent John</span></li>
                        <li>Categories: <span>God, Pray</span></li>
                        <li><span>On Monday 23 DEC, 2018</span></li>
                    </ul>
                    <div class="icon-links">
                        <a href=""><i class="ti-link"></i></a>
                        <a href=""><i class="ti-zip"></i></a>
                        <a href=""><i class="ti-headphone"></i></a>
                        <a href=""><i class="ti-import"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4">
            <div class="sermon-item">
                <div class="si-thumb set-bg" data-setbg="img/sermons/5.jpg" style="background-image: url(&quot;img/sermons/5.jpg&quot;);"></div>
                <div class="si-content">
                    <h4>The Lord, a God Merciful and Gracious</h4>
                    <ul class="sermon-info">
                        <li>Sermon From: <span>Vincent John</span></li>
                        <li>Categories: <span>God, Pray</span></li>
                        <li><span>On Monday 23 DEC, 2018</span></li>
                    </ul>
                    <div class="icon-links">
                        <a href=""><i class="ti-link"></i></a>
                        <a href=""><i class="ti-zip"></i></a>
                        <a href=""><i class="ti-headphone"></i></a>
                        <a href=""><i class="ti-import"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4">
            <div class="sermon-item">
                <div class="si-thumb set-bg" data-setbg="img/sermons/6.jpg" style="background-image: url(&quot;img/sermons/6.jpg&quot;);"></div>
                <div class="si-content">
                    <h4>The Lord Whose Name Is Jealous</h4>
                    <ul class="sermon-info">
                        <li>Sermon From: <span>Vincent John</span></li>
                        <li>Categories: <span>God, Pray</span></li>
                        <li><span>On Monday 23 DEC, 2018</span></li>
                    </ul>
                    <div class="icon-links">
                        <a href=""><i class="ti-link"></i></a>
                        <a href=""><i class="ti-zip"></i></a>
                        <a href=""><i class="ti-headphone"></i></a>
                        <a href=""><i class="ti-import"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="pagination-area">
        <ul class="pageination-list">
            <li><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">Next <i class="fa fa-angle-double-right"></i></a></li>
        </ul>
        <p>Page 1 of 08 results</p>
    </div>
</div>