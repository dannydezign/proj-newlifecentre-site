	<!-- Footer top section -->
	<section class="footer-top-section spad">
		<div class="container">
			<div class="row">
				<div class="col-sm-6 footer-top-content">
					<div class="section-title title-left">
						<h2>Contact Us</h2>
					</div>
					<h3>Mere Green Community Centre</h3>
					<p>30 Mere Green Rd,</p>
					<p>Sutton Coldfield B75 5BT</p>
					<p><span>Email:</span> newlifecentresc@gmail.com</p>
					<p><span>Phone:</span> +12234567</p>
				</div>
			</div>
		</div>
		<!-- googel map -->
		<div class="map-area" id="map-canvas"></div>
	</section>
	<!-- Footer top section end-->
	<!-- Footer section -->
	<footer class="footer-section">
		<div class="container">
			<div class="row">
				<div class="col-sm-6 copyright">
					<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | Newlife Centre Church
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
				</div>
				<div class="col-sm-6">
					<div class="social">
						<a href="#"><i class="fab fa-facebook-f"></i></a>
					   	<a href="#"><i class="fab fa-twitter"></i></a>
					   	<a href="#"><i class="fab fa-google-plus-g"></i></a>
					   	<a href="#"><i class="fab fa-instagram"></i></a>
					</div>
				</div>
				 
			</div>
		</div>
	</footer>
	<!-- Footer section end -->