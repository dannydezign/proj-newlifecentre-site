<?php

namespace App\Http\Services;
use App\Models\App;
use App\Models\AppConfigs;


class Navigation
{
   
    public static function getNavigation() {    
        
        $nav = [
            [
                'name' => 'home',
                'link' => 'home',
                'route' => 'laravel',
                'show' => true,
            ],
            [
                'name' => 'about',
                'link' => 'about',
                'route' => 'laravel',
                'show' => true,
            ],
            [
                'name' => 'sermons',
                'link' => 'sermons',
                'route' => 'laravel',
                'show' => true,
            ],
            [
                'name' => 'events',
                'link' => 'events',
                'route' => 'laravel',
                'show' => true,
            ],
            [
                'name' => 'giving',
                'link' => 'giving',
                'route' => 'laravel',
                'show' => false,
            ],
            [
                'name' => 'holy spirit experience',
                'link' => 'holyspiritexperience',
                'route' => 'laravel',
                'show' => false,
            ],
            [
                'name' => 'Join Us Live',
                'link' => self::getYoutubeLink(),
                'route' => 'youtube',
                'show' => true,
            ]
        ];

        return $nav;
    }

    protected static function getYoutubeLink()
    {
        $id = App::where('name', 'navigation')->pluck('id')->first();
        return AppConfigs::where('app_id', $id)->where('name', 'youtube-link')->pluck('value')->first();
    }
}
