@extends('layouts.main')

@section('content')

    @include('includes.title-section', ['title' => 'about us'])

    @include('partials.about', ['aboutUs' =>  'about us', 'aboutUsGroup' => $data])
 
    @include('partials.gallery', $images)

    
@endsection